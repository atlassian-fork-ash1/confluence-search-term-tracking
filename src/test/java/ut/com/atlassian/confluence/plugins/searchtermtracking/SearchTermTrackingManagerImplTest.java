package ut.com.atlassian.confluence.plugins.searchtermtracking;

import java.util.ArrayList;
import java.util.List;

import com.atlassian.confluence.event.events.search.SearchPerformedEvent;
import com.atlassian.confluence.plugins.searchtermtracking.SearchTermTrackingManager;
import com.atlassian.confluence.plugins.searchtermtracking.SearchTermTrackingManagerImpl;
import com.atlassian.confluence.plugins.searchtermtracking.events.SearchTermPerformedEvent;
import com.atlassian.confluence.search.v2.SearchQuery;
import com.atlassian.confluence.search.v2.query.TextFieldQuery;
import com.atlassian.event.api.EventPublisher;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SearchTermTrackingManagerImplTest
{
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private SearchPerformedEvent event;
    @Mock
    private SearchQuery searchQuery;

    private SearchTermTrackingManager manager;

    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
        when(event.getSearchQuery()).thenReturn(searchQuery);
        manager = new SearchTermTrackingManagerImpl(eventPublisher);
    }

    @Test
    public void testAfterPropertiesSet() throws Exception
    {
        manager.afterPropertiesSet();
        verify(eventPublisher).register(manager);
    }

    @Test
    public void testDestroy() throws Exception
    {
        manager.destroy();
        verify(eventPublisher).unregister(manager);
    }

    @Test
    public void testHandle_Title()
    {
        String rawQuery = "This is the raw query";
        List<SearchQuery> parameters = new ArrayList<SearchQuery>();
        TextFieldQuery titleSearchQuery = mock(TextFieldQuery.class);
        parameters.add(titleSearchQuery);
        when(searchQuery.getParameters()).thenReturn(parameters);
        when(titleSearchQuery.getFieldName()).thenReturn("title");
        when(titleSearchQuery.getRawQuery()).thenReturn(rawQuery);

        manager.handle(event);

        verify(eventPublisher).publish(any(SearchTermPerformedEvent.class));
    }

    @Test
    public void testHandle_Body()
    {
        String rawQuery = "This is the raw query";
        List<SearchQuery> parameters = new ArrayList<SearchQuery>();
        TextFieldQuery titleSearchQuery = mock(TextFieldQuery.class);
        parameters.add(titleSearchQuery);
        when(searchQuery.getParameters()).thenReturn(parameters);
        when(titleSearchQuery.getFieldName()).thenReturn("body");
        when(titleSearchQuery.getRawQuery()).thenReturn(rawQuery);

        manager.handle(event);

        verify(eventPublisher).publish(any(SearchTermPerformedEvent.class));
    }

    @Test
    public void testHandle_WithNullParams()
    {
        when(searchQuery.getParameters()).thenReturn(null);

        manager.handle(event);

        verify(eventPublisher, never()).publish(any(SearchTermPerformedEvent.class));
    }

    @Test
    public void testHandle_WithNonTextFieldQuery()
    {
        List<SearchQuery> parameters = new ArrayList<SearchQuery>();
        SearchQuery nonTextFieldQuery = mock(SearchQuery.class);
        parameters.add(nonTextFieldQuery);
        when(searchQuery.getParameters()).thenReturn(parameters);

        manager.handle(event);

        verify(eventPublisher, never()).publish(any(SearchTermPerformedEvent.class));
    }

    @Test
    public void testHandle_NonTitleOrBody()
    {
        List<SearchQuery> parameters = new ArrayList<SearchQuery>();
        TextFieldQuery titleSearchQuery = mock(TextFieldQuery.class);
        parameters.add(titleSearchQuery);
        when(searchQuery.getParameters()).thenReturn(parameters);
        when(titleSearchQuery.getFieldName()).thenReturn("space");

        manager.handle(event);

        verify(eventPublisher, never()).publish(any(SearchTermPerformedEvent.class));
    }

}
