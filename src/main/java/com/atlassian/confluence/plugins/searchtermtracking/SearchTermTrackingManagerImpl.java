package com.atlassian.confluence.plugins.searchtermtracking;

import java.util.List;

import com.atlassian.confluence.event.events.search.SearchPerformedEvent;
import com.atlassian.confluence.plugins.searchtermtracking.events.SearchTermPerformedEvent;
import com.atlassian.confluence.search.v2.SearchQuery;
import com.atlassian.confluence.search.v2.query.TextFieldQuery;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;

import org.apache.commons.lang3.StringUtils;

public class SearchTermTrackingManagerImpl implements SearchTermTrackingManager
{
    private final EventPublisher eventPublisher;

    public SearchTermTrackingManagerImpl(EventPublisher eventPublisher)
    {
        this.eventPublisher = eventPublisher;
    }

    @EventListener
    @Override
    public void handle(SearchPerformedEvent e)
    {
        List<SearchQuery> parameters = e.getSearchQuery().getParameters();
        if (parameters != null)
        {
            for (SearchQuery param : parameters)
            {
                if (param instanceof TextFieldQuery)
                {
                    TextFieldQuery textParam = (TextFieldQuery) param;
                    if (StringUtils.equals(textParam.getFieldName(), "title") ||
                            StringUtils.equals(textParam.getFieldName(), "body"))
                    {
                        eventPublisher.publish(new SearchTermPerformedEvent(textParam.getRawQuery()));
                        return;
                    }
                }
            }
        }
    }

    @Override
    public void destroy() throws Exception
    {
        eventPublisher.unregister(this);
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        eventPublisher.register(this);
    }
}
